README.TXT for

/*
File: spellcheck.cpp and QuadraticProbing.h and QuadraticProbing.cpp
Name: adass
Course: CSCI 335, Hunter College
*/

Assignment: 

Hashing Application 
Implement a spell checker by using the hash table with quadratic probing. You can use the words.txt file 
from the previous assignment as your dictionary. Given a document your program should output all of 
the misspelled words. For each misspelled word you should also provide a list of candidate corrections 
that can be formed by applying one of the following rules to the misspelled word: 
a)  Adding one character 
b)  Removing one character 
c)  Swapping adjacent characters 
Your program should run as follows: 
spellcheck <document file> <dictionary file> 


a. All parts of the assignment were completed. 

b. Cannot recall major issues.

c. "make all" and execute the spellcheck executable like this: "spellcheck <document file> <dictionary file>"
	Documents and dictionaries are provided from the previous as well as current assignments.
	For convenience, output to text for reading like this: "spellcheck <document file> <dictionary file> > output.txt"

Try "spellcheck turing.txt wordsen.txt > output.txt".
"turing_key.txt" provides a sample of spelling errors that the program should catch.