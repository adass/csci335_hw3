/*
File: spellcheck.cpp
Name: adass
Course: CSCI 335, Hunter College

About: This is a spellcheck routine that uses a hash table with quadratic probing.
A document and words (dictionary) file are read. Words in the document are checked
against the dictionary. Misspelled words are listed, along with suggestions following
three correction strategies:
    1) Adding a character to the word
    2) Removing a character from the word
    3) Swapping adjacent characters in the word

QuadraticProbing.h and QuadraticProbing.cpp remain exactly the same as provided.

Run the program like this: spellcheck <document file> <dictionary file>
Because the output can get long, depending on your terminal, you may wish to
save the output to a file instead, like this:
    
    spellcheck <document file> <dictionary file> > output.txt


=== Checklist ===
1) Read command line arguments
2) Insert words/dictionary into table
3) List misspellings ***
4) Suggest words found by adding one character
5) Suggest words found by removing one character
6) Suggest words found by swapping adjacent characters
7) Format output properly

***
    a) Even though the assignment doesn't ask for it explicitly, the search is 
        case-insensitive in order to accomodate for say, "Septembre" (the dictionary 
        is all lower case as far as I can tell, so "Septembre" needs to be converted 
        to lower case in order to get the spelling correction- "september")...
        Of course, this is just an example! ALL document queries are automatically
        lower cased for capitalized words other than September (even common words like
        "It" that are capitalized will be seen as misspellings otherwise...)
    b) An extra character of punctuation is seen as a misspelling. However,
        neither the assignment PDF nor the Blackboard announcement require that
        to be treated differently. So, attached punction = misspelling.
    c) Type of correction (add character, remove character, swap two characters)
        is NOT listed next to each correction (or same type sequence of corrections)
        because a) it's not required by the assignment PDF specs
                b) adds too much clutter to the formatting of the output
*/

#include <iostream>
#include <fstream>
#include <string>
#include <sstream> // for string splitting
#include <cstring> // for strcmp on systems that require it
#include <algorithm> // for swap and transform algorithms
#include <iomanip> // for setw

#include "QuadraticProbing.h"

using namespace std;

// forward declarations
void insertInTable(HashTable<string>& table, ifstream& wordsFile, size_t& maxLen);
void notInTable(HashTable<string>& table, ifstream& docFile, const size_t maxLen);

void addChar(HashTable<string>& table, const string& query);
void remChar(HashTable<string>& table, const string& query);
void swapAdj(HashTable<string>& table, const string& query);


/*
    main:
        main takes two arguments (the document to check, the dictionary to check
        against), outputs some introductory text, builds an empty table, calls a
        function to populate that table, checks for the misspellings through another
        function call, outputs some concluding text, then exits.
*/
int main(int argc, const char* argv[])
{
    // command line argument processing - based on my own "Assignment 2"
    string docTxtName = argv[1];
    string wordsTxtName = argv[2];
    
    // make sure both arguments have been supplied
    if (argc != 3)
    {
        cerr << "Please execute spellcheck properly:\n" << endl;
        cerr << "spellcheck <document file> <dictionary file>" << endl;
        return EXIT_FAILURE; // exit with error?
    }
    
    cout << "\n====================================================\n";
    cout << "Welcome to spellcheck! --- Written by: adass ---" << endl;
    cout << "====================================================\n\n";
    cout << "----------------------------------------------------\n";
    cout << "Configurations: \n\n";

    // process "first" argument from argv[1], document file name
    ifstream docFileStream;
    docFileStream.open(docTxtName.c_str());
    if (docFileStream.fail()) {cerr << "\n\n\t!!!Couldn't read document file- try again!!!\n\n\n"; return(1);} // exit with error?
    cout << "Document          : " << docTxtName << endl;

    // process "second" argument from argv[2], words file name
    ifstream wordsFileStream;
    wordsFileStream.open(wordsTxtName.c_str());
    if (wordsFileStream.fail()) {cerr << "\n\n\t!!!Couldn't read words file- try again!!!\n\n\n"; return(1);} // exit with error?
    cout << "Words (dictionary): " << wordsTxtName << endl;    

    cout << "----------------------------------------------------\n" << endl;;
    
    size_t maxLen = 0; // max length of DICTIONARY words, for 'setw' formatting purposes
    
    // build empty hash table, then insert dictionary into it
    HashTable<string> wordTable;
    insertInTable(wordTable, wordsFileStream, maxLen);
    
    // check if document words are in table (this function calls the other functions
    // that perform the character manipulations (add, remove, swap))
    // this function, and the functions it calls, perform the outputs themselves
    notInTable(wordTable, docFileStream, maxLen);
    
    cout << "\n\n\nThanks for using spellcheck!\n";

} // end main

/*
    insertInTable:
        insertInTable takes three arguments:
            a) reference to table
            b) reference to dictionary file
            c) reference to maxLen variable
        This function populates referenced table with words parsed from referenced
        dictionary file, while updating the referenced maxLen variable to update 
        the max word length for use in other functions.
*/
void insertInTable(HashTable<string>& table, ifstream& wordsFile, size_t& maxLen)
{
    string line;
    
    cout << "Reading words from 'words' file into hash table..." << endl;
    while (getline(wordsFile, line))
    {
        table.insert(line);
        if (maxLen < line.length()) {maxLen = line.length();}
    } // end getline

    // reset wordsFile
    wordsFile.clear();
    wordsFile.seekg(0, ios::beg);

} // end insertInTable

/*
    notInTable:
        notInTable takes three arguments:
            a) reference to table
            b) reference to document file
            c) constant value of maxLen variable
        This function parses referenced document file into individual words/strings 
        and looks up each of those words (in lower case!) in referenced table. 
        When a word is not found, it is first output, padded with whitespace based
        on the (now) constant value of maxLen, then sent (in lower case!) to three 
        functions. Each of those functions performs a different manipulation on the 
        string to account for common spelling errors. Each of those functions handles
        its own output. Lower casing done using the simple STL algorithm 'transform'.
*/
void notInTable(HashTable<string>& table, ifstream& docFile, const size_t maxLen)
{
    cout << "You have misspelled the following words: \n\n";

    string border = "";
    border.insert(0, (maxLen * 2), '_'); // insert _ in pos 1 (maxlen * 2) times
    cout << border;
    cout << "\n\nMisspellings" << setw(maxLen - 12) << " \t" << "Corrections\n"; // 12 = length of Misspellings
    cout << border << endl;  
    
    string line;
    stringstream lineStr;
    string strToken;
    string strLowerCase;
    
    while (getline(docFile, line))
    {
        lineStr.str(line);
        
        while (lineStr >> strToken)
        {
            strLowerCase = strToken;
            transform(strLowerCase.begin(), strLowerCase.end(), strLowerCase.begin(), ::tolower); // use transform from <algorithm>
            if (!table.contains(strLowerCase))
            {
                cout << strToken << " " << setw(maxLen - strToken.length()) << "| \t";
                
                //cout << "#Add char: ";
                addChar(table, strLowerCase);
                
                //cout << "#Remove char: ";
                remChar(table, strLowerCase);
                
                //cout << "#Swap adjacents: ";
                swapAdj(table, strLowerCase);
                
                cout << " \n";
            } // end match
        } // end word in doc line
        
        // reset lineStr, or else the loop will only process the first line
        lineStr.clear();
        lineStr.seekg(0, ios::beg);
        
    } // end line in doc
    
    cout << border;
    
} // end notInTable

/*
    addChar:
        addChar takes two arguments:
            a) reference to table
            b) constant reference to misspelled word/string
        This function takes the string argument, copies it, and then manipulates
        the copied string by adding a character to it. This is done by looping
        through each character position in the string, and adding one alphanumeric
        character (both upper and lower cases) by looping through each addable 
        character from a reference string charStr, and checking the manipulated
        string in the table. If found, it is output through this function.
*/
void addChar(HashTable<string>& table, const string& query)
{
    string charStr = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    for (size_t i = 0; i < query.length(); i++) // for every char position in query
    {
        for (size_t j = 0; j < charStr.length(); j++) // for every char in "string" of all relevant chars-- charStr
        {
            string modifiedQuery = query;
            modifiedQuery.insert(i, 1, charStr[j]); // insert char j in query pos i, 1 time
            if (table.contains(modifiedQuery))
            {
                cout << "*" << modifiedQuery << " ";
            } // end match 
        } // end j
    } // end i

} // end addChar

/*
    remChar:
        remChar takes two arguments:
            a) reference to table
            b) constant reference to misspelled word/string
        This function takes the string argument, copies it, and then manipulates
        the copied string by removing a character from it. This is done by looping
        through each character position in the string, removing the character
        in that position, and checking the manipulated string in the table. 
        If found, it is output through this function.
*/
void remChar(HashTable<string>& table, const string& query)
{
    for (size_t i = 0; i < query.length(); i++) // for every char position in query
    {
        string modifiedQuery = query;
        modifiedQuery.erase(i, 1); // 2nd parameter for how many chars to remove
        if (table.contains(modifiedQuery))
        {
            cout << "*" << modifiedQuery << " ";
        } // end match
    } // end i

} // end remChar

/*
    swapAdj:
        swapAdj takes two arguments:
            a) reference to table
            b) constant reference to misspelled word/string
        This function takes the string argument, copies it, and then manipulates
        the copied string by swapping two adjacent characters. This is done by looping
        through each character position in the string EXCEPT for the very last one
        (since we access the i+1 position in a swap), and using the simple STL
        'swap' algorithm to swap the i-th and i+1 chars, and checking the manipulated
        string in the table. If found, it is output through this function.
*/
void swapAdj(HashTable<string>& table, const string& query)
{
    for (size_t i = 0; i < (query.length() - 1); i++) // for every char pos, but not last and beyond- out of bounds!
    {
        string modifiedQuery = query;
        swap(modifiedQuery[i], modifiedQuery[i+1]); // use swap from <algorithm>
        if (table.contains(modifiedQuery))
        {
            cout << "*"  << modifiedQuery << " ";
        } // end match        
    } // end i
    
} // end swapAdj


// end spellcheck.cpp
